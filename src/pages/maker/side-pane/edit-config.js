const props = {
	name: {
		type: String,
		readonly: false
	},
	componentName: {
		type: String,
		readonly: true
	},
	uid: {
		type: String,
		readonly: true
	},
	height: {
		type: Number,
		readonly: false
	},
	layout: {
		type: Array,
		readonly: false,
		values: [
			{
				label: 'vertical',
				value: 'column'
			},
			{
				label: 'horizontal',
				value: 'row'
			}
		]
	}

}

const components = [
	{
		type:'1',
		name:'容器',
		children: [
			{id: 1, icon: 'icon-ai233', classname:'Layout'}
		]
	},{
		type:'2',
		name:'图表',
		children: [
			{id: 1, icon: 'icon-fsux_tubiao_zhuzhuangtu', classname:'Bar'},
			{id: 2, icon: 'icon-fsux_zhexiantu', classname:'Line'},
			{id: 3, icon: 'icon-fsux_tubiao_bingtu1', classname:'Pie'},
			{id: 4, icon: 'icon-fsux_tubiao_bingtu', classname:'Pie'},
			{id: 5, icon: 'icon-fsux_tubiao_leidatu', classname:'Pie'},
		]
	}
]

export {
	props,
	components
}
