import Package from '../package'

class MakerStore {
	constructor(options) {
		this.currentSelectedNode = null;
		this.root = new Package['Layout']({name: 'root_layout', root: true, level: 0});
		for (let key in options) {
			if (options.hasOwnProperty(key)) {
				this[key] = options[key];
			}
		}
	}

	deleteNode() {
		let node = this.currentSelectedNode
		if(node.parent){
			node.parent.children.splice(node.parent.children.findIndex(item => item === node.data), 1)
			this.currentSelectedNode = null;
		}
	}

	addNode(parent, data) {
		let node = new Package[data.classname]()
		parent.children.push(node)
		let json = JSON.stringify(parent.children)
		parent.children = JSON.parse(json)
	}
}

export default MakerStore
