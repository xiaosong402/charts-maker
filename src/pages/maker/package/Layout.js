const LAYOUT = {
	VERTICAL: {
		label: 'vertical',
		value: 'column'
	},
	HORIZONTAL: {
		label: 'horizontal',
		value: 'row'
	}
}

let pointerUID = 0


export default class Layout {
	constructor(options) {
		this.uid = pointerUID++
		this.name = `layout_${this.uid}`;
		this.componentName = 'layout'
		this.root = false
		this.level = 0
		for (let key in options) {
			if (options.hasOwnProperty(key)) {
				this[key] = options[key];
			}
		}
		this.layout =Object.assign({},LAYOUT.VERTICAL)
		this.children = [];
		this.height = 0
	}
}


export class node {
	constructor() {
		this.name = 'layout';
		this.componentName = 'layout';
		this.children = [];
		this.options = {
			layout: LAYOUT.HORIZONTAL
		}
	}
}
