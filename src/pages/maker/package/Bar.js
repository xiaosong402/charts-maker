let pointerUID = 0
export default class Bar {
	constructor(options) {
		this.uid = pointerUID++
		this.name = `bar_${this.uid}`;
		this.componentName = 'bar'
		this.root = false
		this.level = 0
		for (let key in options) {
			if (options.hasOwnProperty(key)) {
				this[key] = options[key];
			}
		}
		this.height = 0
	}
}

