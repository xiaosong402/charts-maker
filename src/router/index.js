import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
	routes: [
		{path: '/', redirect: '/maker'},
		{
			path: '/maker',
			name: 'maker',
			component: () => import('@/pages/maker/maker')
		}
	]
})
