### demo-vdata

项目定位  H5图表编辑生成工具

### 技术选型

1，图表库 antv F2 (针对 移动端优化过的) 
2，拖动库vue-drag-drop （功能 不能完全满足）


> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
